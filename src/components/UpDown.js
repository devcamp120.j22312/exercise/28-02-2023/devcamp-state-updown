

import { toHaveAccessibleDescription } from "@testing-library/jest-dom/dist/matchers";
import { Component } from "react";

class UpDown extends Component {
    constructor(props) {
        super(props)
        this.state = {
            number: 0
        }
    }

    clickUpHandler = () => {
        this.setState({
            number: this.state.number + 1
        })
    }

    clickDownHandler = () => {
        this.setState({
            number: this.state.number - 1
        })
    }
    
    render() {
        return(
            <>
            <button onClick={this.clickUpHandler}>+</button>
            <button onClick={this.clickDownHandler}>-</button>
            <p>Value: {this.state.number}</p>
            </>
        )
    }
}
export default UpDown;